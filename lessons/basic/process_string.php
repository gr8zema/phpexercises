<?php

/* Замена элемента в строке */

/**
 * @param string $string
 *
 * @return string
 */
function processData(string $string): string{
  return str_replace('.', '-', $string);
}
