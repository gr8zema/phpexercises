<?php
/*
Дан текстареа и кнопка. В текстареа вводится текст.
По нажатию на кнопку выведите количество слов в тексте,
количество символов в тексте, количество символов за вычетом пробелов.
 */


if (!empty($_GET['message'])) {
  $message = trim($_GET['message']);
  $string = preg_replace('/\s+/', ' ', $message);
  echo 'Количество слов: ' . count(explode(' ', $string)) . "<br/>";
  echo 'Количество символов с пробелами: ' . mb_strlen($message) . "<br/>";
  echo 'Количество символов: ' . mb_strlen(preg_replace('/\s+/', '', $message)) . "<br/>";
} else {
  echo 'Text Area is empty';
}
