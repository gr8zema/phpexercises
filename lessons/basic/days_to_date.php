<?php

/* Скрипт считает дни до даты*/

if (!empty($_GET['date'])) {
  $dateArr = date_parse($_GET['date']);
  $currentTime = time() - mktime(0, 0, 0, $dateArr['month'], $dateArr['day'], $dateArr['year']);
  $days = floor(abs($currentTime / (24 * 60 * 60)));
  echo $days;
}
else {
  echo 'Enter correct Date!';
}
