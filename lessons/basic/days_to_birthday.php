<html>
<!--Дан инпут и кнопка. В этот инпут вводится дата рождения в формате
'01.12.1990'. По нажатию на кнопку выведите на экран сколько дней
осталось до дня рождения пользователя.-->

<body>

<form action="" method="get">
  Введите Дату: <input type="text" name="date" title="date"><br>
<input type="submit">
</form>

</body>
</html>

<?php

if (isset($_GET['date'])) {
  $dateArr = explode('.', $_GET['date']);
  $currentTime = time() - mktime(0, 0, 0, $dateArr[1], $dateArr[0], $dateArr[2]);
  $days = floor(abs($currentTime / (24 * 60 * 60)));
  echo $days;
}

/* Скрипт считает дни до даты*/
if (!empty($_GET['date'])) {
  $dateArr = date_parse($_GET['date']);
  $currentTime = time() - mktime(0, 0, 0, $dateArr['month'], $dateArr['day'], $dateArr['year']);
  $days = floor(abs($currentTime / (24 * 60 * 60)));
  echo $days;
} else {
  echo 'Enter correct Date!';
}
