<?php

/*
 * Проверить является ли введенный год високосным
 */

/**
 * @param int $year
 *
 * @return bool
 */
/*function is_leap_year(int $year): bool {
   if (date('L', mktime(1, 1, 1, 1, 1, $year)) === 1) {
     return true;
   } else {
     return false;
   }
}*/

function is_leap_year(int $year): bool {
  return ($year % 4 == 0 && ($year % 100 != 0 || $year % 400 == 0)) ? TRUE : FALSE;
}

$res = is_leap_year(2008);
if ($res) {
  echo 'Year is LEAP';
}
else {
  echo 'Year is plain';
}
