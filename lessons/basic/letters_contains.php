<?php

/*
 * Дан массив слов, инпут и кнопка. В инпут вводится набор букв. По нажатию на
кнопку выведите на экран те слова, которые содержат в себе все введенные буквы.
 */

$words = [
  'задача',
  'распределение',
  'некоторые',
  'композиции',
  'дубликация',
  'редактор',
  'вёрстка',
  'качество',
  'юмористический',
  'известные'
];

if (!empty($_GET['message'])) {
  $input = mb_strtolower(trim($_GET['message']));
  $flag = FALSE;
  $output = [];
  foreach ($words as $value) {
    for ($i = 0; $i < mb_strlen($input); $i++) {
      if (stristr($value, mb_substr($input, $i, 1)) === FALSE) {
        $flag = FALSE;
        break;
      }
      else {
        $flag = TRUE;
      }
    }
    if ($flag) {
      $output[] = $value;
    }
  }
  if (!empty($output)) {
    foreach ($output as $word) {
      echo 'Найдено ' . "\"$word\"" . "<br/>";
    }
  }
  else {
    echo 'Нифига не найдено';
  }
}
else {
  echo 'Text area is empty';
}
