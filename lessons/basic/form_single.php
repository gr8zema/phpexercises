<?php
/*
 * Модифицируем предыдущую задачу: пусть теперь на один вопрос может быть несколько правильных ответов.
 * Пользователь должен отметить один или несколько чекбоксов.
 */

$data = [
  'Выберите опреационные системы?' => ['right' => ['Android', 'Ubuntu'],
                                'options' => ['Western Union', 'Doom']],
  'Перечислите планеты нашей солнечной системы?' => ['right' => ['Марс', 'Венера'],
                            'options' => ['Солнце', 'Антарес']],
  'Из чего состоит молекула воды?' => ['right' => ['Водород', 'Кислород'],
                                'options' => ['Азот', 'Хлорид Натрия']],
  'Назовите имя пресонажа из популярного романа Артура Конан-Дойля?' => ['right' => ['Шерлок', 'Доктор Мориарти'],
                                                                        'options' => ['Доктор Хаус', 'Тарзан']]
        ];
echo "<pre>";
var_dump($data);
echo "</pre>";
?>
<!doctype html>
<html>
<body>
<?php if ($_SERVER['REQUEST_METHOD'] == 'GET'): ?>

  <h2>Отвечайте на вопросы!</h2>

  <?php $counter = 1;?>
  <form action="form_single.php" method="POST">
<?php foreach ($data as $question => $answer): ?>

      <p><?= $question; ?></p>

  <?php foreach ($answer as $key => $value): ?>
    <?php foreach ($value as $option): ?>
    <label>
      <input type="checkbox" name="<?='answer' . $counter;?>[]" value="<?= $option;?>">
      <?= $option;?>
    </label><br>
    <?php endforeach;?>
  <?php endforeach;?>
  <?php $counter++;?>

  <?php endforeach;?>
  <br>
  <input type="submit" name="submit" value="Отправить">
  </form>

<?php endif; ?>
</body>
</html>

<?php

echo "<pre>";
var_dump($_POST);
echo "</pre>";
if (isset($_POST['submit'])) {
  $counter = 1;
  foreach ($data as $question => $val) {
    echo $question;
    $answer = $_POST['answer' . $counter] ?? '';
    if ($answer == $val['right']) {
      echo "<p style=\"color:forestgreen;\">Ваш ответ \"$answer\" верный</p><br/>";
    } else {
      echo "<p style=\"color:red;\">Ответ \"$answer\" НЕ верный.</p><br/>";
    }
    $counter++;
  }
  echo "<a href=\"$_SERVER[SCRIPT_NAME]\">Вернуться к форме</a>";
}

?>




