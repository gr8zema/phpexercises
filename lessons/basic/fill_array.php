<?php

/**
 * @param int $start
 * @param int $end
 *
 * @return array
 */
function fillArray(int $start, int $end): array {
  $result = [];
  for ($i = $start; $i <= $end; $i++) {
    $result[] = $i;
  }

  return $result;
}
