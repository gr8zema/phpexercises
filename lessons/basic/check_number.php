<?php

/*
 * Проверить существует ли в массиве значение
 */

/**
 * @param array $array
 * @param int $number
 *
 * @return string
 */
function check_number(array $array, int $number): string {
  foreach ($array as $item) {
    if ($item === $number) {
      return "Number $number exists";
    }
  }

  return "Number $number doesn't exist";
}
