<?php

/* Заполнить массив случайными цифрами */

/**
 * @param int $quant
 *
 * @return array
 */
function randomArrayValues(int $quant): array {
  $result = [];
  for ($i = 1; $i <= $quant; $i++) {
    $result[] = mt_rand(1, 100);
  }

  return $result;
}

