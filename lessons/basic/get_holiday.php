<?php

/*
Дан массив праздников. По заходу на страницу, если сегодня праздник,
то поздравьте пользователя с этим праздником.
*/

/**
 * @return mixed|null
 */
function getHoliday() {
  $holidays = [

    '01-01' => 'Новый год',
    '07-01' => 'Рождество Христoво',
    '08-03' => 'Международный женский день',
    '08-04' => 'Пасха',
    '09-05' => 'День Победы',
    '27-05' => 'Троица',
    '28-06' => 'День Конституции Украины',
    '24-08' => 'День Независимости Украины',
    '14-10' => 'День защитника Украины',
    '25-12' => 'Рождество Христово католическое'
  ];
  $currentDate = date('d-m');

  return isset($holidays[$currentDate]) ? $holidays[$currentDate] : NULL;
}
