<?php

/* По заходу на страницу выведите сколько дней осталось до ближайшей масленницы
(последнее воскресенье весны). */

/**
 * @return int
 */
function days_until_maslennica(): int {

  $currentTime = time() - strtotime('last sunday of May');
  $days = floor(abs($currentTime / (24 * 60 * 60)));

  return 'До масленницы осталось ' . $days . ' дней.';
}


