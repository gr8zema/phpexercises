<?php

/* дан массив с числами найди сумму элементов этого массива */

/**
 * @param array $array
 *
 * @return int
 */
function summa(array $array): int {
  $summ = 0;
  foreach ($array as $number) {
    $summ += $number;
  }

  return $summ;
}

