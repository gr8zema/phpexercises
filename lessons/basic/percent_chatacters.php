<?php

/*Дан текстареа и кнопка. В текстареа вводится текст.
По нажатию на кнопку нужно посчитать процентное содержание каждого символа в тексте.
*/

if (!empty($_GET['message'])) {
  $message = mb_strtolower(trim($_GET['message']));
  $str = mb_strlen($message, 'UTF-8');
  $storage = [];
  for ($i = 0; $i < $str; $i++) {
    $char = mb_substr($message, $i, 1, 'UTF-8');
    if (!array_key_exists($char, $storage)) {
      $storage[$char] = 0;
    }
    $storage[$char]++;
  }
  $output = [];
  foreach ($storage as $key => $value) {
    $output[$key] = round(($value / $str * 100), 2, PHP_ROUND_HALF_EVEN);
    echo "Символ '$key' составляет - $output[$key] %" . "<br/>";
  }
}
else {
  echo 'Text Area is empty';
}
