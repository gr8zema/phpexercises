<?php

/*По заходу на страницу выведите сколько дней осталось до нового года. */

/**
 * @param int $year
 *
 * @return string
 */
function daysUntilNewYear(int $year): string {
  $currentTime = time() - mktime(23, 59, 59, 12, 31, $year);
  $days = floor(abs($currentTime / (24 * 60 * 60)));


  return "Days until New Year $days";
}
