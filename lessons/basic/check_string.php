<?php

/* Валидация строки на http и https */

/**
 * @param string $str
 *
 * @return string
 */
function check_string(string $str): string {
  if (substr($str, 0, 7) === 'http://' || substr($str, 0, 8) === 'https://') {
    return 'string is valid';
  }

  return 'string is invalid';
}

