<!--Дан инпут и кнопка. В этот инпут вводится дата рождения в формате '31.12'.
По нажатию на кнопку выведите знак зодиака пользователя.-->
<html>
<body>
<h4>Узнайте свой знак Зодиака.</h4>
<form action="" method="GET">
  Введите Дату в формате (число.месяц):
  <input type="text" name="date" title="date"><br>
  <input type="submit">
</form>

</body>
</html>

<?php
if (isset($_GET['date'])) {
  $date = explode('.', $_GET['date']);
  $month = $date[1];
  $day = $date[0];

  function getZodiac($month, $day) {
    // массив с названиями знаков зодиака
    $zodiacName = [
      "Козерог",
      "Водолей",
      "Рыбы",
      "Овен",
      "Телец",
      "Близнецы",
      "Рак",
      "Лев",
      "Девы",
      "Весы",
      "Скорпион",
      "Стрелец",
    ];
    // массив дней, с которых сменяется знак зодиака
    $zodiacDate = [
      21,
      20,
      20,
      20,
      20,
      20,
      21,
      22,
      23,
      23,
      23,
      23,
    ];
    if ($day < $zodiacDate[$month - 1]) {
      $result = $zodiacName[$month - 1];
    }
    else {
      if ($month == 12) {
        $month = 0;
      }
      $result = $zodiacName[$month];
    }

    return $result;
  }

  echo "Ваш знак зодиака - " . getZodiac($month, $day);
}
else {
  echo 'Введите дату.';
}
