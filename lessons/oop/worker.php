<?php
class User {
  private $name;
  private $age;

  public function __construct($name, $age) {
    $this->name = $name;
    $this->age = $age;
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name): void {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getAge() {
    return $this->age;
  }

  /**
   * @param mixed $age
   */
  public function setAge($age): void {
    if ($this->checkAge($age)) $this->age = $age;
  }

  private function checkAge($age): bool {
    if ($age > 1 && $age < 100) return true;

    return false;
  }
}

class WorkerClass extends User {

  private $salary;

  /**
   * @return mixed
   */
  public function getSalary() {
    return $this->salary;
  }

  /**
   * @param mixed $salary
   */
  public function setSalary($salary): void {
    $this->salary = $salary;
  }

}

class Driver extends WorkerClass{

  private $experience;
  private $category;

  /**
   * @return mixed
   */
  public function getExperience() {
    return $this->experience;
  }

  /**
   * @param mixed $experience
   */
  public function setExperience($experience): void {
    $this->experience = $experience;
  }

  /**
   * @return mixed
   */
  public function getCategory() {
    return $this->category;
  }

  /**
   * @param mixed $category
   */
  public function setCategory($category): void {
    if (preg_match('/[A-Ca-c]/', $category) !== 0) $this->category = $category;
  }


}




